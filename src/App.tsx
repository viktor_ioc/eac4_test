import gitLabLogo from '/public/gitlab-icon.svg'
import './App.css'

function App() {


  return (
    <>
      <div>
       
        <a href="https://react.dev" target="_blank">
          <img src={gitLabLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Viktor Lazarenko</h1>

      <p className="read-the-docs">
        Estem aprenent a fer us del sistema de control de versions Git i de la plataforma GitLab.
      </p>
    </>
  )
}

export default App
